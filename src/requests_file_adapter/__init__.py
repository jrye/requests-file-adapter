#!/usr/bin/env python3
"""
Convenience stub for common usage.
"""

from .adapter import FileAdapter
from .utils import get_session

__all__ = ["FileAdapter", "get_session"]
